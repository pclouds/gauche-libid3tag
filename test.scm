;;;
;;; Test id3tag
;;;

(use gauche.test)

(test-start "id3tag")
(use id3tag)
(test-module 'id3tag)

(define file #f)
(define tag #f)
(define frame #f)

(define (test-is-a expected actual)
  (is-a? actual expected))

(test* "open in read only mode" <id3-file>
       (set! file (id3-file-open "the_eternal_flame-god_wrote_in_lisp.mp3"))
       test-is-a)

(test* "get tag" <id3-tag>
       (set! tag (id3-file-tag file))
       test-is-a)

(test* "id3-tag-version" #x400 (id3-tag-version tag))

(test* "id3-tag-get-frame-count" 7
       (id3-tag-get-frame-count tag))

(test* "id3-tag-frame-id-list"
       '("WXXX" "COMM" "TCON" "TIT2" "TPE1" "TALB" "TCOM")
       (id3-tag-frame-id-list tag))

(test* "id3-tag-frame" "TPE1"
       (id3-frame-get-id
        (set! frame (id3-tag-frame tag 4))))

(test* "id3-frame-get-field-count" 2
       (id3-frame-get-field-count frame))

(test* "id3-field-text-encoding" 'utf-16
       (id3-field-text-encoding
        (id3-frame-field frame 0)))

(test* "id3-field-get-strings" '("Julia Ecklar")
       (id3-field-get-strings
        (id3-frame-field frame 1)))

(test* "id3-field-get-latin1" ""
       (id3-field-get-latin1
        (id3-frame-field (id3-tag-find-frame tag "WXXX" 0) 2)))

;; If you don't want `gosh' to exit with nonzero status even if
;; the test fails, pass #f to :exit-on-failure.
(test-end :exit-on-failure #t)
