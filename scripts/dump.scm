#!/usr/bin/env gosh
(use gauche.uvector)
(use id3tag)
(use srfi-1)
(use srfi-151)

(define (main args)
  (let* ([file (id3-file-open (second args))]
         [tag (id3-file-tag file)])
    (print "Version "
           (arithmetic-shift (id3-tag-version tag) -8)
           "."
           (bitwise-and (id3-tag-version tag) #xff))
    (print)

    (map (lambda (fr)
           (print (id3-frame-get-id fr)
                  " - "
                  (id3-frame-get-description fr))
           (map (lambda (fi)
                  (print "  [" (id3-field-type fi) "] "
                         (id3-field-get-value fi))
                  )
                (map (cut id3-frame-field fr <>)
                     (iota (id3-frame-get-field-count fr))))
           (print))
         (map (cut id3-tag-frame tag <>)
              (iota (id3-tag-get-frame-count tag))))))
