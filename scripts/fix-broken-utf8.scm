#!/usr/bin/env gosh
(use file.util)
(use gauche.parseopt)
(use gauche.uvector)
(use gauche.unicode)
(use id3tag)
(use srfi-1)

(define (ucs4->u8vec vec)
  (call/cc
   (lambda (exit)
     (list->u8vector
      (map
       (lambda (x)
         (when (or (< x 0) (> x 255))
           (exit #f))
         x)
       (u32vector->list vec))))))

(define (fixup frame update?)
  (call/cc
   (lambda (exit)
     (unless (is-a? frame <id3-frame>)
       (exit #f))
     (let* ([enc-field (id3-frame-field frame 0)]
            [str-field (id3-frame-field frame 1)]
            [ucs4 (id3-field-get-ucs4-strings str-field)]
            [utf8 (apply string-append (id3-field-get-strings str-field))])
       (if (and (eq? (id3-field-text-encoding enc-field) 'iso-8859-1) (length=? ucs4 1))
           (guard (exc [else #f])
            (let ([new-utf8 (utf8->string (ucs4->u8vec (first ucs4)))])
              (unless (string=? utf8 new-utf8)
                (if update?
                    (begin
                      (id3-field-set-text-encoding! enc-field 'utf-8)
                      (id3-field-set-strings! str-field (list new-utf8)))
                    (print "Fixing " utf8 " => " new-utf8))
                (exit #t))))))
     #f)))

(define (main args)
  (let-args (cdr args)
      ((update? "u|update") . restargs)
    (unless (length=? restargs 1)
      (error "a single mp3 path is required"))
    (let* ([path (first restargs)]
           [bak-path (string-append path ".bak")])
      (when update?
        (copy-file path bak-path :if-exists :supersede))
      (let* ([file (id3-file-open path :update update?)]
             [tag [id3-file-tag file]]
             [frames (filter-map (cut id3-tag-find-frame tag <> 0)
                                 '("TIT2" "TPE1" "TALB"))]
             [results (filter-map (cut fixup <> update?) frames)])
        (if update?
            (if (null? results)
                (delete-file bak-path)
                (begin
                  (id3-file-update! file)
                  (print path " has been fixed up")))
            (unless (null? results)
              (print path " needs fixup")))))))
