;;
;; Package gauche-libid3tag
;;

(define-gauche-package "gauche-libid3tag"
  :version "0.1"
  :description "libid3tag bindings"
  :require (("Gauche" (>= "0.9.7"))
            #;("libid3tag" (>= "0.15.1b")))
  :providing-modules (id3tag)
  :authors ("Duy Nguyen <pclouds@gmail.com>")
  :licenses ("MIT")
  :repository "https://gitlab.com/pclouds/gauche-libid3tag")
