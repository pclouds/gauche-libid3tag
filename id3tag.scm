;;;
;;; gauche_libid3tag
;;;

(define-module id3tag
  (use gauche.uvector)
  (export-all))
(select-module id3tag)

;; Loads extension
(dynamic-load "gauche_libid3tag")

(define field-type-map
  `((,ID3_FIELD_TYPE_TEXTENCODING . text-encoding)
    (,ID3_FIELD_TYPE_LATIN1 . latin1)
    (,ID3_FIELD_TYPE_LATIN1FULL . latin1-full)
    (,ID3_FIELD_TYPE_LATIN1LIST . latin1-list)
    (,ID3_FIELD_TYPE_STRING . string)
    (,ID3_FIELD_TYPE_STRINGFULL . string-full)
    (,ID3_FIELD_TYPE_STRINGLIST . string-list)
    (,ID3_FIELD_TYPE_LANGUAGE . language)
    (,ID3_FIELD_TYPE_FRAMEID . frame-id)
    (,ID3_FIELD_TYPE_DATE . date)
    (,ID3_FIELD_TYPE_INT8 . int8)
    (,ID3_FIELD_TYPE_INT16 . int16)
    (,ID3_FIELD_TYPE_INT24 . int24)
    (,ID3_FIELD_TYPE_INT32 . int32)
    (,ID3_FIELD_TYPE_INT32PLUS . int32-plus)
    (,ID3_FIELD_TYPE_BINARYDATA . binary-data)))

(define text-encoding-map
  `((,ID3_FIELD_TEXTENCODING_ISO_8859_1 . iso-8859-1)
    (,ID3_FIELD_TEXTENCODING_UTF_16 . utf-16)
    (,ID3_FIELD_TEXTENCODING_UTF_16BE . utf-16be)
    (,ID3_FIELD_TEXTENCODING_UTF_8 . utf-8)))

(define (number->id3-field-type type)
  (let ([pair (assq type field-type-map)])
    (if (pair? pair)
        (cdr pair)
        (errorf "field type ~a unrecognized" type))))

(define (number->id3-text-encoding enc)
  (let ([pair (assq enc text-encoding-map)])
    (if (pair? pair)
        (cdr pair)
        (errorf "text encoding ~a unrecognized" enc))))

(define (id3-text-encoding->number enc)
  (let ([pair (assq enc (map (lambda (x)
                               (cons (cdr x) (car x)))
                             text-encoding-map))])
    (if (pair? pair)
        (cdr pair)
        (errorf "text encoding ~a unrecognized" enc))))

(define (id3-file-open path :key (update #f))
  (id3-file-open-low-level path
                           (if update
                               ID3_FILE_MODE_READWRITE
                               ID3_FILE_MODE_READONLY)))

(define (id3-field-get-value field)
  (case (id3-field-type field)
    ((text-encoding) (id3-field-text-encoding field))
    ((latin1) (id3-field-get-latin1 field))
    ((latin1-full) (id3-field-get-full-latin1 field))
    ((string) (id3-field-get-string field))
    ((string-full) (id3-field-get-full-string field))
    ((string-list) (id3-field-get-strings field))
    ((binary-data) (id3-field-get-binary-data field))
    ((frame-id) (id3-field-get-frame-id field))
    ((language) (id3-field-get-language field))
    ((date) (id3-field-get-date field))
    ((int8 int16 int24 int32) (id3-field-get-int field))
    ((int32-plus) (id3-field-get-int32-plus field))
    (else (errorf "unsupported field type ~a" (id3-field-type field)))))

(define (id3-field-type field)
  (number->id3-field-type (id3-field-type-low-level field)))

(define (id3-field-text-encoding field)
  (number->id3-text-encoding (id3-field-text-encoding-low-level field)))

(define (id3-field-get-string field)
  (u32vector->string (id3-field-get-ucs4-string field)))

(define (id3-field-get-full-string field)
  (u32vector->string (id3-field-get-full-ucs4-string field)))

(define (id3-field-get-strings field)
  (map u32vector->string (id3-field-get-ucs4-strings field)))

(define (id3-field-get-language field)
  (unless (eq? (id3-field-type-low-level field) ID3_FIELD_TYPE_LANGUAGE)
    (error "not a language field"))
  (id3-field-get-immediate field))

(define (id3-field-get-date field)
  (unless (eq? (id3-field-type-low-level field) ID3_FIELD_TYPE_DATE)
    (error "not a date field"))
  (id3-field-get-immediate field))

(define (id3-field-set-text-encoding! field encoding)
  (id3-field-set-text-encoding-low-level! field (id3-text-encoding->number encoding)))

(define (%id3-string->u32vectorz string)
  (u32vector-append (string->u32vector string) #u32(0)))

(define (id3-field-set-string! field string)
  (id3-field-set-ucs4-string! field (%id3-string->u32vectorz string)))

(define (id3-field-set-strings! field strings)
  (id3-field-set-ucs4-strings! field (map %id3-string->u32vectorz strings)))
